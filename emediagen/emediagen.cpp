// emediagen.cpp : Defines the entry point for the console application.
//
// addytywny generator LCG

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <ctime>

using namespace std;

typedef unsigned long long ulong;

// zmienne globalne dla generatora
const ulong a = 3141592653ull;
const ulong c = 2718281829ull;
const ulong m = 34359738368ull;

ulong X0;

void Seed()  // inicjowanie ziarna
{
	X0 = (ulong)time(NULL);
}

ulong Multiply(ulong a, ulong b, ulong n)  // obliczanie funkcji a*b % n
{
	ulong m, w;

	w = 0; m = 1;
	while (m)
	{
		if (b & m) w = (w + a) % n;
		a = (a << 1) % n;
		m <<= 1;
	}
	return w;
}

ulong LCG()  // generator
{
	X0 = Multiply(X0, a, m);
	X0 = (X0 + c) % m;
	return X0;
}

int main()
{
	ulong Wxy, Lxy, x, y;
	int n;
	double XR;

	Seed();
	
	/*cout << "Podaj zakres od: ";
	cin >> x;
	cout << "\nPodaj zakres do: ";
	cin >> y;
	cout << "\nPodaj ile liczb: ";
	cin >> n;*/

	x = 0;
	y = INT16_MAX;
	n = 10000;

	Lxy = y - x + 1;

	for (int i = 0; i < n; i++)
	{
		XR = LCG() / (double)m;
		Wxy = (ulong)floor(XR * Lxy) + x;
		cout << Wxy << endl;
	}

	cout << endl << endl;

	system("pause");

	return 0;
}